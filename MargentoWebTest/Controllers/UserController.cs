﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MargentoWebTest.Models;
using System.Linq.Dynamic;
using System.Data.Entity;
using Newtonsoft.Json;
using System.Data;

namespace MargentoWebTest.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddNew()
        {
            return View();
        }
        public ActionResult Edit()
        {
            return View();

        }
        public ActionResult Create()
        {
            return View();

        }
        [HttpPost]
        public ActionResult Edit(Users editUser)
        {
            DatabaseContext _context = new DatabaseContext();
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Entry(editUser).State = EntityState.Modified;
                    _context.SaveChanges();
                    return RedirectToAction("Index", "Home");     
                }
            }
            catch (DataException /* dex */)
            {
                return View(editUser);
            }
            return View(editUser);
        }

       
        [HttpPost]
        public ActionResult Create(Users user)
        {
         
            DatabaseContext _context = new DatabaseContext();
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Users.Add(user);
                    _context.SaveChanges();
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (DataException /* dex */)
            {
                //ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View("Index", "Home");
        }
       
        [HttpGet]
        public ActionResult Edit(int? ID)
        {
            try
            {
                using (DatabaseContext _context = new DatabaseContext())
                {
                    var User = (from user in _context.Users
                                    where user.id == ID
                                    select user).FirstOrDefault();

                    return View(User);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
        [HttpPost]
        public JsonResult DeleteUser(int? ID)
        {
            using (DatabaseContext _context = new DatabaseContext())
            {
                var user = _context.Users.Find(ID);
                if (ID == null)
                    return Json(data: "Not Deleted", behavior: JsonRequestBehavior.AllowGet);
                _context.Users.Remove(user);
                _context.SaveChanges();

                return Json(data: "Deleted", behavior: JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult LoadData()
        {
            try
            {
                //Creating instance of DatabaseContext class  
                using (DatabaseContext _context = new DatabaseContext())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault();
                    var start = Request.Form.GetValues("start").FirstOrDefault();
                    var length = Request.Form.GetValues("length").FirstOrDefault();
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();


                    //Paging Size (10,20,50,100)    
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // Getting all User data    
                    var userData = (from tempUsers in _context.Users
                                    select tempUsers);

                    //Sorting    
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        userData = userData.OrderBy(sortColumn + " " + sortColumnDir);
                    }
                    //Search    
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        userData = userData.Where(m => m.Name == searchValue);
                    }

                    //total number of rows count     
                    recordsTotal = userData.Count();
                    //Paging     
                    var data = userData.Skip(skip).Take(pageSize).ToList();
                    //Returning Json Data    
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                }
            }
            catch (Exception)
            {
                throw;
            }


        }
    }
}