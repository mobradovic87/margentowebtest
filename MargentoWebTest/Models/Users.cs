﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MargentoWebTest.Models
{
    [Table("User")]
    public class Users
    {
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage = "Required Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Required LastName")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Required BirhtDate")]
        public System.DateTime BirhtDate { get; set; }

        [Required(ErrorMessage = "Required PhoneNumber")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Required Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Required PostCode")]
        public int PostCode { get; set; }

        [Required(ErrorMessage = "Required City")]
        public string City { get; set; }
    }
}