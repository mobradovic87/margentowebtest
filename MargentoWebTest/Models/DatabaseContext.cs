﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MargentoWebTest.Models;

namespace MargentoWebTest.Models
{
    public class DatabaseContext: DbContext
    {
        public DbSet<Users> Users { get; set; }
    }
}